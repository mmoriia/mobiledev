// Сортировка массива с помощью замыкания
var numbers = [5, 2, 8, 1, 9, 3]

// Сортировка в одну сторону
numbers.sort(by: { $0 < $1 })
print(numbers) // [1, 2, 3, 5, 8, 9]

// Сортировка в обратную сторону
numbers.sort(by: { $0 > $1 })
print(numbers) // [9, 8, 5, 3, 2, 1]

// Метод для сортировки массива имен по количеству букв
func sortFriendsByLength(_ friends: [String]) -> [String] {
    return friends.sorted(by: { $0.count < $1.count })
}

// Пример использования метода
let friends = ["John", "Mary", "Alex", "Sarah"]
let sortedFriends = sortFriendsByLength(friends)
print(sortedFriends) 

// Создание словаря из отсортированного массива имен
var dictionary: [Int: String] = [:]
for friend in sortedFriends {
    dictionary[friend.count] = friend
}
print(dictionary) 

// Функция для вывода ключа и значения словаря
func printDictionaryValue(for key: Int) {
    if let value = dictionary[key] {
        print("Key: \(key), Value: \(value)")
    } else {
        print("No value found for key \(key)")
    }
}

// Пример использования функции
printDictionaryValue(for: 5) 

// Функция для проверки массивов на пустоту и вывода в консоль
func checkArrays(_ array1: [String], _ array2: [Int]) {
    if array1.isEmpty {
        let defaultName = "John"
        print("Array 1 is empty. Adding default name: \(defaultName)")
        var newArray1 = array1
        newArray1.append(defaultName)
        print(newArray1)
    } else {
        print(array1)
    }
    
    if array2.isEmpty {
        let defaultValue = 0
        print("Array 2 is empty. Adding default value: \(defaultValue)")
        var newArray2 = array2
        newArray2.append(defaultValue)
        print(newArray2)
    } else {
        print(array2)
    }
}

// Пример использования функции
let array1 = ["John", "Mary", "Alex"]
let array2: [Int] = []
checkArrays(array1, array2)
